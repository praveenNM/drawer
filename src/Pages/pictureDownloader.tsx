import React, { useEffect, useState } from 'react'
import Nav from "../components/nameselectiondrawercomponent/nav"
import { RhSlider,RhBreadCrumbs,RhBreadCrumbsItem,RhIcon,RhButton,RhDivider,RhTabs,RhTabsItem,RhPopover,RhCard,RhPopoverMenu,RhPopoverToggle,RhListContainer,RhListItem,RhListItemText,RhButtonGroup,RhImage } from '@rhythm-ui/react'


export default function pictureDownloader() {
  const [width,setWidth]=useState(900)
    const data=[
        { src: "https://media.istockphoto.com/id/1146517111/photo/taj-mahal-mausoleum-in-agra.jpg?s=612x612&w=0&k=20&c=vcIjhwUrNyjoKbGbAQ5sOcEzDUgOfCsm9ySmJ8gNeRk=", alt: "alt text", id: "1",width:width }]

    const [isBig,setIsBig]=useState(false);
    const [input,setInput]=useState(0);
    

    const handleSize=()=>{
      isBig?setIsBig(false):setIsBig(true);
      isBig?setWidth(1400):setWidth(900)
    }
    
    const handleSliderChange = (val) => {
        setInput(val[0])  
        
    setWidth(900+(val[0]+val[0])+20) 
    console.log(width);  
      };
  return (
    <div>
<Nav/>
<div className='mx-4'>
<div className='mt-10'>
            <RhBreadCrumbs variant="regular" className="text-gray-500 mt-2"  separator={<RhIcon icon="bi:slash-lg" />}>
  <RhBreadCrumbsItem
    icon={<RhIcon className="text-gray-400" />}
    label="Project"
  />
  <RhBreadCrumbsItem
    icon={<RhIcon className="text-gray-400" />}
    label="Project 4"
  />
  <RhBreadCrumbsItem
    icon={
      <RhIcon className="text-gray-400" />
    }
    label="Session 2"
  />
</RhBreadCrumbs>
</div>
<div className='flex justify-between'>
        <div> 
    <h1 className='text-2xl mt-2'>Session 2</h1>
    <p className=''>Updated on 16 May 2023, 10:40 am by Ganit Kumar</p>
    </div>
    
    </div>  <div>
    <RhTabs
  variant="underline"
  stretched={false}
  vertical={false}
  onTabClick={(value) => {
    console.debug(value);
  }}
>
  <RhTabsItem title="Material" value="1" />
  <RhTabsItem title="Output" value="2" />
</RhTabs>
            <RhDivider className='w-full'></RhDivider>
        </div>
    </div>
    <div className='flex flex-row justify-between m-4'>
        <div>
        <RhButtonGroup>
    <RhButton variant="white">Point Cloud</RhButton>
    <RhPopover placement="bottom-end">
      <RhPopoverToggle asChild>
        <RhButton variant="white" icon="heroicons:chevron-down-20-solid" />
      </RhPopoverToggle>
      <RhPopoverMenu>
        <RhCard>
          <RhListContainer>
            <RhListItem onClick={() => null}>
              <RhListItemText primary="Point Cloud 1" />
            </RhListItem>
            <RhListItem onClick={() => null}>
              <RhListItemText primary="Point Cloud 2" />
            </RhListItem>
            <RhListItem onClick={() => null}>
              <RhListItemText primary="Point Cloud 3" />
            </RhListItem>
          </RhListContainer>
        </RhCard>
      </RhPopoverMenu>
    </RhPopover>
  </RhButtonGroup>
        </div>
      
        <div className='flex justify-center gap-1'>
    <RhButton variant="white" icon="pepicons-pop:file" />
    <RhButton variant="white" icon="lucide:video" />
    <RhButton variant="white"  onClick={(()=>handleSize())} >{isBig?<RhIcon icon='akar-icons:reduce' className='text-xl'/>:<RhIcon icon='entypo:resize-full-screen' className='text-xl'/>}</RhButton>
    <RhButton variant="white" icon="ic:sharp-share" />
    <RhButton >Download</RhButton>
        </div>
    </div>
    <div className='grid place-content-center'>
        {data.map(ele=>(
            <div className='flex flex-wrap'>
              <RhImage
              src={ele.src}
             alt={ele.alt}
             width={`${width}px`}
             height="full"
           />
           </div >
        ))}
  
    </div>
    <div className='mx-8 fixed bottom-0 right-0 left-0 bg-white'>
    <RhSlider min={0} max={100} value={[input]} onChange={(e)=>handleSliderChange(e)} />

    </div>
    </div>
  )
}
