import { RhDivider, RhImage, RhButton, RhInputFormik } from "@rhythm-ui/react";
import { Formik, Form } from "formik";
import React from "react";
import { Link } from "react-router-dom";
const Registaion = () => {
	return (
		<div className="h-screen w-screen fixed top-0  ">
			<header className=" h-[58.5px] w-full ">
				<RhImage
					alt="Image-description"
					height="37px"
					src="./logo.svg"
					width="72px"
					className="dark:bg-inherit m-0 ml-[40px] mt-[13.5px]"
				/>
				<RhDivider className="mt-4 p-0" />
			</header>
			<div className=" h-[94%] w-full flex flex-col items-center mt-8   ">
				<div className="h-[779px] w-[90%]  sm:w-[60%]  lg:w-[30%] xl:w-[30%] border-[1px] border-[#CCCCCC]  rounded-[8px]  ">
					<RhImage
						alt="Image-description"
						src="./Group 2.svg"
						width="100%"
						className="m-0 mt-4"
					/>
					<div className="h-[520px] w-full  pr-[24px] pl-[24px] mt-[24px] ">
						<h4 className="m-0">Register Profile</h4>
						<h6 className="">Enter bellow details to register your profile</h6>
						<Formik
							initialValues={{
								name: "",
								email: "",
								mobile_number: "",
								password: "",
								confirm_password: "",
							}}
							onSubmit={(values) => {
								alert(JSON.stringify(values));
							}}
						>
							<Form className="flex flex-col mt-4 ">
								<>
									<div className=" h-[76px]">
										<RhInputFormik
											label="Name"
											className="w-full h-[48px]  rounded-[8px] p-[12px]  "
											name="name"
											isOptional
											placeholder="Enter name"
										/>
									</div>

									<div className=" h-[76px] mt-4">
										<RhInputFormik
											label="E-mail"
											className="w-full h-[48px]  rounded-[8px] p-[12px] "
											name="email"
											isOptional
											placeholder="Enter Email ID"
										/>
									</div>
									<div className=" h-[76px] mt-4">
										<RhInputFormik
											label="Mobile Number"
											className="w-full h-[48px]  rounded-[8px] p-[12px] "
											name="mobile_number"
											isOptional
											placeholder="Enter Mobile Number"
										/>
									</div>
									<div className=" h-[76px] mt-4">
										<RhInputFormik
											label="Password"
											className="w-full h-[48px]  rounded-[8px] p-[12px] "
											name="password"
											isOptional
											placeholder="Enter here"
										/>
									</div>
									<div className=" h-[76px] mt-4">
										<RhInputFormik
											label="Confirm Password"
											className="w-full h-[48px]  rounded-[8px] p-[12px] "
											name="confirm_password"
											isOptional
											placeholder="Enter here"
										/>
									</div>
								</>

								<div className="flex  mt-[24px]  gap-2 w-full">
									<Link to="/" className="w-full">
										<RhButton
											className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#13679F] "
											type="submit"
											size="xl"
										>
											CANCEL
										</RhButton>
									</Link>
									<Link to="/confirmemail" className="w-full">
										<RhButton
											className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#F2F2F2] hover:text-[#B3B3B3]"
											type="submit"
											size="xl"
										>
											REGISTER NOW
										</RhButton>
									</Link>
								</div>
							</Form>
						</Formik>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Registaion;
