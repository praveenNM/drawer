import Modal from "../components/nameselectiondrawercomponent/modal/modal";
import { RhButton, RhDrawer } from "@rhythm-ui/react";
import { useState } from "react";

import { List } from "../components/nameselectiondrawercomponent/card/list";
import { boolean } from "yup";
const Drawer = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [projectname, setProjectname] = useState("");
  const [description, setDescription] = useState("");
  const [userlist, setuserlist] = useState(List);
  function Opendrawer() {
    setIsOpen(!isOpen);
  }
  function consoleinfo() {
    console.log(`projectname:${projectname}`);
    console.log(`Description:${description}`);
    let users = userlist.filter((user) => {
      if (user.selected) {
        return user;
      }
    });
    {
      if (users.length == 0) {
        console.log("zero memebrs are selected for the project");
      } else {
        console.log("the selected memebrs for the project");
        userlist.map((user) => {
          if (user.selected) {
            console.log(user.name);
          }
        });
      }
    }
    setProjectname("");
    setDescription("");
    setIsOpen(false);
  }
  return (
    <>
      <div className={"flex items-center justify-center "}>
        <RhButton onClick={Opendrawer}>Click to open the drawer</RhButton>
        <RhDrawer
          variant="temporary"
          position="right"
          size="medium"
          closeOnOutsideClick={true}
          backdrop={true}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
          className="flex flex-col p-3"
        >
          <Modal
            projectname={projectname}
            setProjectname={setProjectname}
            description={description}
            setDescription={setDescription}
            userlist={userlist}
            setuserlist={setuserlist}
          />
          <div className="flex justify-between pt-2">
            <RhButton
              onClick={() => setIsOpen(false)}
              layout="outline"
              variant="danger"
            >
              Cancel
            </RhButton>
            <RhButton variant="success" onClick={consoleinfo}>
              Add Projects
            </RhButton>
          </div>
        </RhDrawer>
      </div>
    </>
  );
};

export default Drawer;
