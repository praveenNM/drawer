import React from "react";
import { Link } from "react-router-dom";
import {
	RhImage,
	RhButton,
	RhDivider,
	RhLabel,
	RhInput,
	RhFormGroup,
	RhFormGroupItem,
	RhInputGroup,
	RhIcon,
} from "@rhythm-ui/react";
const Login = () => {
	const LOGO_URL =
		"https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg";
	const COVER_URL =
		"https://images.unsplash.com/photo-1505904267569-f02eaeb45a4c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1908&q=80";
	return (
		<div className=" h-screen w-screen grid grid-cols-1 lg:grid-cols-2  fixed top-0  ">
			<div className="hidden lg:block  h-screen w-[90%] ">
				<RhImage
					alt=""
					src="./banner.jpg"
					// contain={true}
					className="mt-0 h-screen w-full object-fit  object-cover"
				/>
			</div>
			<div className=" flex w-full ">
				<div className=" w-full flex flex-col justify-center items-center   ">
					<RhImage
						alt="Image-description"
						height="48px"
						src="./logo.svg"
						width="94px"
						className="dark:bg-inherit"
					/>
					<div className="mt-1  flex flex-wrap flex-col  items-center justify-center w-[90%] sm:w-[50%] p-2  ">
						<div className=" w-full flex justify-start">
							<h2 className="text-md text-black mb-8">Login</h2>
						</div>
						<div className="space-y-1 w-full mb-6">
							<RhLabel labelFor="email" className="w-full">
								Email ID
							</RhLabel>
							<RhInput
								id="email"
								type="email"
								block
								placeholder="Email ID"
								className="text-sm  h-[48px] rounded-[8px]"
							/>
						</div>

						<div className="space-y-1 w-full mb-[20px] ">
							<RhLabel labelFor="password">Password</RhLabel>
							<RhInputGroup className=" h-[48px] rounded-[8px] text-sm">
								<RhInput
									id="password"
									type="password"
									block
									placeholder="Password"
								/>
								<RhIcon
									icon="mdi:eye-off-outline"
									className="text-2xl text-[#808080]"
								/>
							</RhInputGroup>
						</div>

						<div className=" w-full flex justify-end mb-8   ">
							<Link
								to="/forgotpassword"
								className="text-primary-500  font-semibold cursor-pointer decoration-transparent "
							>
								Forgot password?
							</Link>
						</div>
						<Link to="/projectfilter" className="w-full">
							<RhButton
								className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#13679F] "
								type="submit"
								size="xl"
							>
								LOGIN
							</RhButton>
						</Link>
						<div className="mt-12">
							<span className="mr-2">Not a member?</span>
							<Link
								to="/Registration"
								className="decoration-transparent text-[#4152B7]"
							>
								Register Now
							</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;
