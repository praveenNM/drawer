import React from "react";
import Nav2 from "../components/nameselectiondrawercomponent/nav2";
// import Searchbar from "../components/projectFilter/searchBar";
import Projectinfo from "../components/projectFilter/projectinfo";
const projectFilter = () => {
  return (
    <div className=" w-screen overscroll-none fixed top-0 p-4">
      <Nav2 />
      <div className="h-[90%] ">
        {/* <Searchbar /> */}
        <Projectinfo />
      </div>
    </div>
  );
};

export default projectFilter;
