import React from "react";
import {
	RhImage,
	RhDivider,
	RhCard,
	RhCardBody,
	RhLabel,
	RhInput,
	RhButton,
} from "@rhythm-ui/react";
import { Link } from "react-router-dom";
const resetPassword = () => {
	return (
		<div className="h-screen w-screen fixed top-0 ">
			<header className=" h-[58.5px] w-full ">
				<RhImage
					alt="Image-description"
					height="37px"
					src="./logo.svg"
					width="72px"
					className="dark:bg-inherit m-0 ml-[40px] mt-[13.5px]"
				/>
				<RhDivider className="mt-4 p-0" />
			</header>{" "}
			<div className=" h-[90%] relative w-full flex flex-col items-center mt-16 ">
				<RhCard className="h-[435px] w-[580px]  lg:w-[30%]  border-[1px] border-[#F2F2F2] ">
					<RhCardBody className="  p-0  mt-4 ">
						<div className="relative flex justify-between h-[131px] w-full ">
							<div className="absolute top-[26px] left-[58px] ">
								<RhImage alt="reloading" src="./lock.svg" className="m-0 " />
							</div>
							<div className="absolute top-[62.74px] left-[271.36px] ">
								<RhImage
									alt="reloading"
									src="./password.svg"
									className="m-0 "
								/>
							</div>
							<div className="absolute top-[12px] right-[24px]">
								<RhImage
									alt="reloading"
									src="./enhanced_encryption.svg"
									className="m-0 "
								/>
							</div>
						</div>
						<div className="h-[176px]  mt-[24px] pr-[24px] pl-[24px]">
							<h4 className=" font-[700] m-0 text-[20px]  ">Reset Password</h4>
							<p className="p-0 mt-2 mb-4 text-[14px] font-[400] text-[#333333]">
								Please wait, we will notify you onEnter the email associated
								with your account and we’ll send an email with instructions to
								reset your password.
							</p>
							<div className="space-y-1 w-full mb-[24px] ">
								<RhLabel labelFor="email" className="w-full">
									Email ID
								</RhLabel>
								<RhInput
									id="email"
									type="email"
									block
									placeholder="Enter Email ID"
									className="text-sm  h-[48px] rounded-[8px]"
								/>
							</div>
							<div className="flex    gap-2 w-full">
								<Link to="/" className="w-full">
									<RhButton
										className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#F2F2F2] hover:text-[#B3B3B3]"
										type="submit"
										size="xl"
									>
										CANCEL
									</RhButton>
								</Link>
								<Link to="/emailsent" className="w-full">
									<RhButton
										className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#F2F2F2] hover:text-[#B3B3B3]"
										type="submit"
										size="xl"
									>
										SEND EMAIL
									</RhButton>
								</Link>
							</div>
						</div>
					</RhCardBody>
				</RhCard>
			</div>
		</div>
	);
};

export default resetPassword;
