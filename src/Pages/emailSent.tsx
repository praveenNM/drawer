import React from "react";
import {
	RhImage,
	RhDivider,
	RhCard,
	RhCardBody,
	RhLabel,
	RhInput,
	RhButton,
} from "@rhythm-ui/react";
import { Link } from "react-router-dom";
const EmailSent = () => {
	return (
		<div className="h-screen w-screen fixed top-0 ">
			<header className=" h-[58.5px] w-full ">
				<RhImage
					alt="Image-description"
					height="37px"
					src="./logo.svg"
					width="72px"
					className="dark:bg-inherit m-0 ml-[40px] mt-[13.5px]"
				/>
				<RhDivider className="mt-4 p-0" />
			</header>{" "}
			<div className=" h-[90%] relative w-full flex flex-col items-center mt-16 ">
				<RhCard className="h-[276px] w-[580px]    border-[1px] border-[#F2F2F2] ">
					<RhCardBody className="  p-0  mt-4 ">
						<div className="flex justify-center ">
							<RhImage
								alt="reloading"
								src="./check_circle.svg"
								className="m-0"
							/>
						</div>
						<div className="h-[176px]  mt-[24px] pr-[24px] pl-[24px] ">
							<h4 className=" font-[700] m-0 text-[20px] text-center  ">
								Reset Password
							</h4>
							<p className="p-0 mt-2 mb-4 text-[14px] font-[400] text-[#333333]">
								We have send an Email to your official Email ID name@mail.com,
								please find the mail and follow the instructions
							</p>

							<div className="flex    gap-2 w-full">
								<Link to="/" className="w-full">
									<RhButton
										className="w-full rounded-[8px] bg-[#13679F] hover:bg-[#13679F]"
										type="submit"
										size="xl"
									>
										OKAY
									</RhButton>
								</Link>
							</div>
						</div>
					</RhCardBody>
				</RhCard>
			</div>
		</div>
	);
};

export default EmailSent;
