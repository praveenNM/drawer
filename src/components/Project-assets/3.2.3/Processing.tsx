import React from 'react'
import { RhBreadCrumbs,RhBreadCrumbsItem,RhIcon,RhButton,RhTabs,RhTabsItem,RhCard,RhCardBody } from '@rhythm-ui/react'
import Nav from '../../nameselectiondrawercomponent/nav'
export default function Processing() {
  return (
    <div>
        <Nav/>
        <div className='mx-6'>
        <div className='mt-16'>
        <RhBreadCrumbs
  theme="regular"
  variant="regular"
  key="regular"
  className="text-gray-500"
  block={false}
  separator={<RhIcon icon="bi:slash-lg" />}
>
  <RhBreadCrumbsItem
    label="projects"
  />
  <RhBreadCrumbsItem
    label="the skylight Tower"
  />
  <RhBreadCrumbsItem
   
    label="Session 4"
  />

</RhBreadCrumbs>
        </div>
        <div className='flex flex-row items-center justify-between'>
    <div>
    <h2 className='m-0'>Session 4</h2>
    </div>
    <div>
      <RhButton><RhIcon icon='iwwa:option'/></RhButton>
    </div>
    </div>
    <div>

    <RhTabs
  variant="underline"
  stretched={false}
  vertical={false}
  onTabClick={(value) => {
    console.debug(value);
  }}
>
  <RhTabsItem title="Material" value="1" />
  <RhTabsItem title="Output" value="2" />

</RhTabs>
    </div>
        </div>
        <div className='mt-4'>
        <RhCard className="w-[600px] h-[500px] mx-auto">
  <RhCardBody className="text-justify">


   

    <div className='flex flex-row fixed bottom-0 left-0 right-0 p-4  gap-1' >
        <RhButton className='w-full'>CANCEL PROCESSING</RhButton>
        <RhButton className='w-full'>BACK TO HOME</RhButton>
    </div>
  </RhCardBody>
</RhCard>
        </div>
    </div>
  )
}
