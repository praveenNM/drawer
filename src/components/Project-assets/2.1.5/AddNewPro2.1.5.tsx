import React,{useState} from 'react'
import Nav from '../../nameselectiondrawercomponent/nav'
import { RhBreadCrumbs,RhBreadCrumbsItem,RhButton,RhIcon } from '@rhythm-ui/react'

export default function AddNewProject() {

const [ProjectName,setProjectName]=useState("The Skylight Tower")
const [ProjectDesc,setProjectDesc]=useState("Building project showcases innovation, elegance, and sustainability, setting new standards in design and construction for a brighter, more sustainable future.")
const [Session,setSession]=useState([]) 

return (
    <div>
    <div>
        <Nav/>  
    </div>
    <div className='mx-6'>
    <div className='mt-16'>
    <RhBreadCrumbs
  theme="regular"
  variant="regular"
  key="regular"
  className="text-gray-500"
  block={false}
  separator={<RhIcon icon="bi:slash-lg" />}
>
  <RhBreadCrumbsItem
    label="Project"
  />
  <RhBreadCrumbsItem
    label={ProjectName}
  />
  
</RhBreadCrumbs>
    </div>
    <div className='flex flex-row items-center justify-between'>
    <div>
      <h3 className='m-0'>{ProjectName}</h3>
      <p className='w-[60%]'>{ProjectDesc}</p>
    </div>
    <div>
      <RhButton><RhIcon icon='iwwa:option'/></RhButton>
    </div>
    </div>
    </div>
    <div>
{Session.length===0 ? 
<div className='mt-6 '>
<div className='flex justify-center'><svg width="136" height="164" viewBox="0 0 236 264" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M175.181 131.586C175.181 94.0327 143.43 73.374 107.161 63.5893C60.6617 51.0445 26.606 76.4295 39.1418 131.586C41.9829 144.087 48.7783 158.688 57.1897 171.177H123.938C146.234 156.666 175.181 160.735 175.181 131.586Z" fill="#339EE6"/>
<path d="M140.779 31.4424V137.241C140.779 146.597 133.165 154.209 123.805 154.209H17.9718C8.61253 154.209 0.998535 146.597 0.998535 137.241V31.4424C0.998535 22.0862 8.61253 14.4747 17.9718 14.4747H123.805C133.165 14.4747 140.779 22.0862 140.779 31.4424ZM123.805 152.213C132.077 152.213 138.782 145.51 138.782 137.241V83.9016C138.782 46.6606 108.582 16.4709 71.3288 16.4709H17.9718C9.70057 16.4709 2.99539 23.1738 2.99539 31.4424V137.241C2.99539 145.51 9.70057 152.213 17.9718 152.213H123.805Z" fill="#333333"/>
<path d="M58.3521 236.043H24.1853C23.3239 236.043 22.623 235.343 22.623 234.482C22.623 233.621 23.3239 232.92 24.1853 232.92H58.3521C59.2135 232.92 59.9141 233.621 59.9141 234.482C59.9141 235.343 59.2135 236.043 58.3521 236.043Z" fill="#333333"/>
<path d="M216.104 3.12317H181.937C181.076 3.12317 180.375 2.42251 180.375 1.56142C180.375 0.700327 181.076 0 181.937 0H216.104C216.965 0 217.666 0.700327 217.666 1.56142C217.666 2.42251 216.965 3.12317 216.104 3.12317Z" fill="#333333"/>
<path d="M216.104 36.4234H181.937C181.076 36.4234 180.375 35.7227 180.375 34.8617C180.375 34.0006 181.076 33.3002 181.937 33.3002H216.104C216.965 33.3002 217.666 34.0006 217.666 34.8617C217.666 35.7227 216.965 36.4234 216.104 36.4234Z" fill="#333333"/>
<path d="M234.438 19.7842H163.603C162.741 19.7842 162.041 19.0835 162.041 18.2224C162.041 17.3613 162.741 16.661 163.603 16.661H234.438C235.3 16.661 236 17.3613 236 18.2224C236 19.0835 235.3 19.7842 234.438 19.7842Z" fill="#333333"/>
<path d="M163.727 264H15.9905C7.17329 264 0 256.829 0 248.015V194.149C0 185.334 7.17329 178.163 15.9905 178.163H163.727C172.544 178.163 179.717 185.334 179.717 194.149V248.015C179.717 256.829 172.544 264 163.727 264ZM15.9905 180.16C8.27457 180.16 1.99686 186.435 1.99686 194.149V248.015C1.99686 255.729 8.27458 262.004 15.9905 262.004H163.727C171.443 262.004 177.72 255.729 177.72 248.015V194.149C177.72 186.435 171.443 180.16 163.727 180.16H15.9905Z" fill="#333333"/>
<path d="M220.449 171.176H202.618C172.373 171.176 147.768 146.579 147.768 116.344V98.5198C147.768 92.3528 152.786 87.336 158.955 87.336H220.449C226.617 87.336 231.636 92.3528 231.636 98.5198V159.993C231.636 166.16 226.617 171.176 220.449 171.176ZM158.955 89.3322C153.879 89.3322 149.764 93.4454 149.764 98.5193V116.344C149.764 145.525 173.428 169.18 202.618 169.18H220.449C225.524 169.18 229.639 165.067 229.639 159.993V120.839C229.639 103.438 215.528 89.3322 198.121 89.3322H158.955Z" fill="#333333"/>
<path d="M106.163 202.578C109.96 195.342 114.774 190.122 120.078 186.148H57.5601C73.6506 209.125 94.9894 223.871 106.163 202.578Z" fill="#339EE6"/>
</svg>
</div>
<div className='text-center'>
<h3>No Sessions</h3>
<p>You can add session through the mobile app and, capture images</p>
</div></div>
: Session.map((ele)=>(
<div></div>
))}
    </div>
    </div>
  )
}
