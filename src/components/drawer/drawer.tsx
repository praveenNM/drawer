import React, { useState } from 'react';
import { RhDrawer, RhIcon, RhButton, RhListContainer, RhListItem, RhListItemIcon, RhListItemText, RhAvatar, RhInput, RhInputGroup } from '@rhythm-ui/react';



export default function Backdrop() {
    const [search, setSearch] = useState('');
    const List = [
        { name: 'kishan', desc: 'Member | kishan@gmail.com', selected: false },
        { name: 'praveen', desc: 'Member | praveen@gmail.com', selected: false },
        { name: 'nihal', desc: 'Member | Abcd@gmail.com', selected: false },
        { name: 'shreyank', desc: 'Member | shreyank@gmail.com', selected: false },
        { name: 'ajay', desc: 'Member | ajay@gmail.com', selected: false },
        { name: 'shravani', desc: 'Member | shravani@gmail.com', selected: false },
        { name: 'suhas', desc: 'Member | suhas@gmail.com', selected: true },
        { name: 'chandrika', desc: 'Member | chandrika@gmail.com', selected: false },
        { name: 'kavish', desc: 'Member | kavish@gmail.com', selected: false },
        { name: 'ashutosh', desc: 'Member | ashutosh@gmail.com', selected: false },
        { name: 'ganit', desc: 'Member | ganit@gmail.com', selected: false },
    ];
    const [state, setState] = useState(List);

    const handleAddUser = (user) => {
        setState((prevState) =>
            prevState.map((item) =>
                item.name === user.name ? { ...item, selected: true } : item
            )
        );
    };

    const handleDeleteUser = (user) => {
        setState((prevState) =>
            prevState.map((item) =>
                item.name === user.name ? { ...item, selected: false } : item
            )
        );
    };
    const filteredUsers = state.filter((user) => user.name.toLowerCase().startsWith(search.toLowerCase()));

    return (
        <>

                <div className='ml-6 h-16'>
                    <h2 className="text-lg font-bold ">Users</h2>
                    <div className='flex flex-row mx-4 gap-1 justify-between'>
                        <RhInputGroup >
                            <RhIcon icon="ic:twotone-search" className="text-lg text-gray-400" />
                            <RhInput type="text" className='w-80' placeholder='Search user' value={search} onChange={(e) => setSearch(e.target.value)} />
                        </RhInputGroup>
                        <RhButton variant='white' size='sm'><RhIcon icon='ic:round-filter-list' className='text-2xl' ></RhIcon></RhButton>
                    </div>
                </div>
                <div className="container mx-auto mt-8 h-full overflow-scroll">
                    <h2 className="text-lg font-bold m-2 ml-6">Project Users</h2>
                    <RhListContainer className=" p-2">
                        {filteredUsers.filter((user) => user.selected).map((user) => (
                            <RhListItem className="flex items-center justify-between flex-row h-16">

                                <div className='flex flex-row justify-center items-center'>
                                    <RhListItemIcon variant="primary" align="start">
                                        <RhAvatar
                                            name={user.name}
                                            type="text"
                                            size="xl"
                                            className="border-2 border-white dark:border-dark-900 mr-4 "
                                        />
                                    </RhListItemIcon>
                                    <div>
                                        <RhListItemText primary={user.name} secondary={user.desc} />

                                    </div>
                                </div>
                                <div>
                                    <RhListItemIcon onClick={() => handleDeleteUser(user)} variant="secondary">
                                        <RhIcon
                                            icon="heroicons:trash-solid"
                                            className="text-lg text-gray-400 flex self-end"
                                            color='black'
                                        />
                                    </RhListItemIcon>
                                </div>
                            </RhListItem>
                        ))}
                    </RhListContainer>

                    <h2 className="text-lg font-bold m-2 ml-6">All Users</h2>
                    <div className="p-2 w-full h-full ">
                        {filteredUsers.filter((user) => !user.selected).map((user) => (
                            <RhListItem className="flex items-center justify-between flex-row h-16">

                                <div className='flex flex-row justify-center items-center'>
                                    <RhListItemIcon variant="primary" align="start">
                                        <RhAvatar
                                            name={user.name}
                                            type="text"
                                            size="xl"
                                            className="border-2 border-white dark:border-dark-900 mr-4 "
                                        />
                                    </RhListItemIcon>
                                    <div >

                                        <RhListItemText primary={user.name} secondary={user.desc} />

                                    </div>
                                </div>
                                <div>
                                    <RhListItemIcon onClick={() => handleAddUser(user)} variant="secondary">
                                        <RhIcon
                                            icon="ph:plus-fill"
                                            className="text-lg text-gray-400 flex self-end"
                                            color='black'
                                        />
                                    </RhListItemIcon>
                                </div>
                            </RhListItem>
                        ))}
                    </div>
                </div>
        </>
    );
}
