import React, { useState, useEffect } from "react";
import {
  RhInput,
  RhInputGroup,
  RhIcon,
  RhButton,
  RhInputFormik,
} from "@rhythm-ui/react";
import { Form, Formik } from "formik";

const form = ({ setProjectname, projectname, description, setDescription }) => {
  //   function click() {
  //     console.log("hlo");
  //   }
  // useEffect(()=>{
  // const timer=setTimeout(()={
  // setProjectname()
  // },1000)
  // },[projectname])
  return (
    <div className="h-[15%]  w-full flex flex-col  justify-between">
      {/* <h6 className="font-bold  text-sm">Add new Project</h6>
      <RhInputGroup>
        <RhInput
          type="text"
          placeholder="Project name"
          className="w-full h-8 text-sm mb-1"
          onChange={(e) => setProjectname(e.target.value)}
        />
        <RhIcon
          icon="iconamoon:enter-bold"
          className="text-3xl p-0 m-0"
          onClick={click}
        />
      </RhInputGroup>
      <RhInput
        type="text"
        placeholder="description"
        className="w-full h-16 text-md overflow-hidden "
        type="textarea"
        onChange={(e) => setDescription(e.target.value)}
      /> */}
      <h6 className="font-bold  text-sm">Add new Project</h6>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        // onSubmit={(values) => {
        //   console.log(values);
        // }}
      >
        <Form className="flex flex-col ">
          <>
            <RhInputFormik
              //   label="E-mail"
              className="w-full"
              name="email"
              placeholder="project name"
              value={projectname}
              onChange={(e) => setProjectname(e.target.value)}
            />
            <RhInputFormik
              className="w-full"
              value={description}
              placeholder="Description"
              type={"textarea"}
              name="password"
              onChange={(e) => setDescription(e.target.value)}
              //   label="Password"
            />
          </>
          {/* 
          <div className="flex flex-col mt-2  gap-2 w-full">
            <RhButton className="w-full" type="submit" size="xl">
              Log in
            </RhButton>
          </div> */}
        </Form>
      </Formik>
    </div>
  );
};

export default form;
