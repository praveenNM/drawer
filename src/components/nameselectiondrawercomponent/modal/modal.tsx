import React from "react";
import Card from "../card/card";
import Form from "../form/form";
const modal = ({
	projectname,
	setProjectname,
	description,
	setDescription,
	userlist,
	setuserlist,
}: {
	projectname: string;
	setProjectname: Function;
	description: string;
	setDescription: Function;
	userlist: object[];
	setuserlist: Function;
}) => {
	return (
		<div className="w-full h-[96%] bg-white flex flex-col gap-4">
			<Form
				projectname={projectname}
				setProjectname={setProjectname}
				description={description}
				setDescription={setDescription}
			/>
			<Card userlist={userlist} setuserlist={setuserlist} />
		</div>
	);
};

export default modal;
