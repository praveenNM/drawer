import React from "react";
import { RhButton } from "@rhythm-ui/react";
const Button = ({ showDrawer, drawer }) => {
  function isDraweropen() {
    showDrawer(!drawer);
  }
  return (
    <RhButton size="xl" variant="primary" onClick={isDraweropen}>
      click to open the Name Selection Drawer
    </RhButton>
  );
};

export default Button;
