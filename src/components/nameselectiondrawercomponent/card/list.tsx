export const List = [
  { name: "kishan", desc: "Member|kishan@gmail.com", selected: false },
  { name: "praveen", desc: "Member|praveen@gmail.com", selected: false },
  { name: "nihal", desc: "nihal|Abcd@gmail.com", selected: false },
  {
    name: "shreyank",
    desc: "Member|shreyank@gmail.com",
    selected: false,
  },
  { name: "ajay", desc: "Member|ajay@gmail.com", selected: false },
  { name: "shravani", desc: "Member|shravani@gmail.com", selected: false },
  { name: "suhas", desc: "Member|suhas@gmail.com", selected: true },
  {
    name: "chandrika",
    desc: "Member|chandrika@gmail.com",
    selected: false,
  },
  { name: "kavish", desc: "Member|kavish@gmail.com", selected: false },
  { name: "ashutosh", desc: "Member|ashutosh@gmail.com", selected: false },
  { name: "ganit", desc: "Member|ganit@gmail.com", selected: false },
];
