import React, { useState } from "react";
import Nav from "./nav";
import {
  RhAvatar,
  RhImage,
  RhDivider,
  RhDrawer,
  RhIcon,
  RhButton,
  RhBreadCrumbs,
  RhBreadCrumbsItem,
} from "@rhythm-ui/react";
export default function Nav2() {
  const LOGO_URL =
    "https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg";
  const [isOpen, setIsOpen] = useState(false);
  function cancel() {
    setIsOpen(false);
  }
  function isDraweropen() {
    setIsOpen(false);
  }
  return (
    <div className="flex flex-col h-30 w-full mb-2  ">
      <>
       
      </>
      <div className="   bg-white  ">
       <Nav/>
       <div className="mt-16">
        <RhBreadCrumbs
          variant="regular"
          className="text-gray-500 mt-6"
          separator=" "
        >
          <RhBreadCrumbsItem
            label="Projects"
            className=" border-black border-b-2 "
          />
          <RhBreadCrumbsItem label="Users" />
        </RhBreadCrumbs>
        <RhDivider className="w-full mt-2 " />
      </div>
    </div>
    </div>
  );
}
