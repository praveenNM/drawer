import React from "react";
import { RhInputFormik } from "@rhythm-ui/react";
import { Form, Formik } from "formik";
import { number } from "echarts";
const form = ({
  setProjectname,
  projectname,
  description,
  setDescription,
}: {
  setProjectname: Function;
  projectname: String;
  description: string;
  setDescription: Function;
}) => {
  function debounce(fn: Function, d: number) {
    let timer: any;
    return (a: string) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        fn.apply(this, a);
      }, d);
    };
  }
  function projectnaming(a: string) {
    setProjectname(a);
  }
  const naming = debounce(projectnaming, 100);
  function desc(e: object) {
    setDescription(e.target.value);
  }
  return (
    <div className="h-[15%]  w-full flex flex-col  justify-between">
      <h6 className="font-bold  text-sm">Add new Project</h6>
      <Formik
        initialValues={{
          projectname: "",
          description: "",
          // onsubmit();
        }}
      >
        <Form className="flex flex-col ">
          <>
            <RhInputFormik
              className="w-full"
              name="project name"
              placeholder="project name"
              onChange={(e) => naming(e.target.value)}
            />
            <RhInputFormik
              className="w-full"
              value={description}
              placeholder="Description"
              type={"textarea"}
              name="description"
              onChange={(e) => desc(e)}
            />
          </>
        </Form>
      </Formik>
    </div>
  );
};

export default form;
