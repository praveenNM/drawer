import React from "react";
import { RhButton } from "@rhythm-ui/react";
const submit = () => {
  return (
    <div className="  h-16 w-full mb-3 flex  justify-between">
      <RhButton
        layout="outline"
        className="border-2 border-gray-600 text-gray-500 hover:bg-gray-300 hover:text-black"
      >
        cancel
      </RhButton>
      <RhButton
        layout="solid"
        className="bg-black text-white hover:bg-slate-600"
      >
        Add Project
      </RhButton>
    </div>
  );
};

export default submit;
