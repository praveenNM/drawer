import React, { useState } from "react";
import {
  RhBreadCrumbs,
  RhBreadCrumbsItem,
  RhIcon,
  RhAvatar,
  RhAvatarGroup,
  RhPopover,
  RhPopoverMenu,
  RhDivider,
  RhButton,
  RhCard,
  RhListContainer,
  RhListItem,
  RhListItemText,
  RhPopoverToggle,
} from "@rhythm-ui/react";

const Project = () => {
  const [open, setOpen] = useState(false);
  const togglePopover = () => setOpen(!open);
  return (
    <div className="flex   justify-between items-center gap-2 pt-2">
      <div className="w-[75%] h-full flex flex-col justify-between">
        <div className="flex flex-col">
          <RhBreadCrumbs
            theme="regular"
            variant="regular"
            key="regular"
            className="text-gray-500"
            block={false}
            separator={<RhIcon icon="bi:slash-lg" />}
          >
            <RhBreadCrumbsItem
              //   icon={
              //     <RhIcon icon="heroicons:home-20-solid" className="text-gray-400" />
              //   }
              label="Project"
            />
            <RhBreadCrumbsItem
              //   icon={<RhIcon icon="bxs:contact" className="text-gray-400" />}
              label="Project4"
              className="text-black"
            />
            {/* <RhBreadCrumbsItem
          icon={
            <RhIcon
              icon="heroicons:calendar-days-solid"
              className="text-gray-400"
            />
          }
          label="Calendar"
        />
        <RhBreadCrumbsItem
          icon={<RhIcon icon="bxs:note" className="text-gray-400" />}
          label="Notes" */}
          </RhBreadCrumbs>
          <div className="text-2xl text-black font-bold ">Project</div>
        </div>
        <p className="m-0">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolores
          facilis accusantium necessitatibus in animi itaque tenetur, corrupti
          vel voluptas adipisci, veniam magni eaque nesciunt sint laboriosam.
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolores
        </p>
      </div>
      <div className=" w-[25%]  flex items-center justify-end gap-4">
        <div className=" border-gray-400 border-2 h-14 w-40 flex items-center justify-between p-1">
          <RhAvatarGroup
            max={2}
            innerClass={"border-2 border-white "}
            // className="h-10px"
          >
            <RhAvatar
              type="image"
              size="base"
              src="https://images.pexels.com/photos/12871449/pexels-photo-12871449.jpeg?cs=srgb&dl=pexels-skildring-by-andreas-ellegaard-12871449.jpg&fm=jpg"
              className="border-2 border-white dark:border-dark-900 m-0"
            />
            <RhAvatar
              type="image"
              size="base"
              src="https://images.pexels.com/photos/12871449/pexels-photo-12871449.jpeg?cs=srgb&dl=pexels-skildring-by-andreas-ellegaard-12871449.jpg&fm=jpg"
              className="border-2 border-white dark:border-dark-900 m-0"
            />
            <RhAvatar
              type="image"
              size="base"
              src="https://images.pexels.com/photos/12871449/pexels-photo-12871449.jpeg?cs=srgb&dl=pexels-skildring-by-andreas-ellegaard-12871449.jpg&fm=jpg"
              className="border-2 border-white dark:border-dark-900 m-0"
            />
            <RhAvatar
              type="image"
              size="base"
              src="https://images.pexels.com/photos/12871449/pexels-photo-12871449.jpeg?cs=srgb&dl=pexels-skildring-by-andreas-ellegaard-12871449.jpg&fm=jpg"
              className="border-2 border-white dark:border-dark-900 m-0"
            />
          </RhAvatarGroup>
          <RhIcon
            icon="iconamoon:arrow-up-2-thin"
            rotate={1}
            className="text-3xl text-gray-400"
          />
        </div>

        {/* <div className="h-14 w-14  border-gray-400 border-2 flex items-center justify-center">
          <RhIcon
            icon="bi:three-dots-vertical"
            className="text-2xl text-black "
          />
        </div> */}
        <RhPopover isOpen={open}>
          <RhPopoverToggle className="">
            <RhButton
              onClick={togglePopover}
              variant="white"
              className="h-14 w-14"
            >
              <RhIcon
                icon="bi:three-dots-vertical"
                className="text-2xl text-black "
              />
            </RhButton>
          </RhPopoverToggle>
          <RhPopoverMenu className="  pr-2 h-10 w-40 ">
            <RhCard>
              <RhListContainer>
                <RhListItem onClick={() => null}>
                  <RhListItemText
                    primary={
                      <a href="#" className="decoration-transparent">
                        Dashboard
                      </a>
                    }
                    className="w-full p-0 m-0 "
                  />
                </RhListItem>
                <RhListItem onClick={() => null}>
                  <RhListItemText
                    primary={
                      <a href="#" className="decoration-transparent">
                        Setting
                      </a>
                    }
                    className="w-full"
                  />
                </RhListItem>
              </RhListContainer>
            </RhCard>
          </RhPopoverMenu>
        </RhPopover>
      </div>
    </div>
  );
};

export default Project;
