import React from "react";
import {
  RhCard,
  RhCardBody,
  RhGalleryItem,
  RhImage,
  RhGallery,
  RhIcon,
  RhChip,
} from "@rhythm-ui/react";
const Session = ({ session, info, name, icon, img1, img2, img3, img4 }) => {
  return (
    <RhCard className="border-2 border-black w-full h-40 mb-1">
      <RhCardBody className="text-justify w-full  flex gap-2 items-center h-40 justify-center p-0">
        <RhCard className=" w-[40%] h-full  ">
          <RhCardBody className="flex flex-col h-full justify-center p-0 w-full">
            <h6 className="text-lg font-bold text-black pl-2">{session}</h6>
            <p className="m-0 text-black pl-2 ">{info}</p>
          </RhCardBody>
        </RhCard>
        <RhCard className="w-[60%] h-full ">
          <RhCardBody className="flex items-center justify-between h-full p-0">
            <div className=" w-[20%] mr-1">
              {name ? (
                <RhChip
                  value={name}
                  className="bg-white border-2 text-lg border-gray-500 h-8 w-30  "
                />
              ) : (
                ""
              )}
            </div>
            <div className="w-[80%]">
              <RhGallery
                className="flex gap-4  place-items-center h-full w-full pr-1 "
                onSelection={() => {}}
                position="center"
              >
                <RhIcon icon={icon} className="text-5xl text-black" />
                {[
                  {
                    src: img1,
                    alt: "alt text",
                    id: "1",
                  },
                  { src: img2, alt: "alt text", id: "2" },
                  { src: img3, alt: "alt text", id: "3" },
                  { src: img4, alt: "alt text", id: "4" },
                ].map((item, index) => (
                  <RhGalleryItem
                    id={item.id}
                    className="p-0 m-0 h-36 flex-1  "
                    key={index}
                  >
                    <RhImage
                      className="m-0 p-0 h-full w-full object-cover "
                      src={item.src}
                    />
                  </RhGalleryItem>
                ))}
              </RhGallery>
            </div>
          </RhCardBody>
        </RhCard>
      </RhCardBody>
    </RhCard>
  );
};

export default Session;
