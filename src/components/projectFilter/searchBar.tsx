import { RhPopoverMenu } from "@rhythm-ui/react";
import { RhPopoverToggle } from "@rhythm-ui/react";
import {
  RhImage,
  RhLabel,
  RhIcon,
  RhInput,
  RhButton,
  RhPopover,
  RhAvatar,
  RhListContainer,
  RhListItem,
  RhDivider,
  RhListItemText,
  RhInputGroup,
  RhGallery,
  RhGalleryItem,
} from "@rhythm-ui/react";
import React, { useState } from "react";

const WithSearch = () => {
  return (
    // <div className="">
    <div className=" w-full flex justify-between mb-2">
      <div className=" flex justify-between items-center w-[30%] ">
        {/* <RhInputGroup className="w-full"> */}
        <div className="flex items-center border-2 border-slate-300 w-[90%]">
          <RhIcon
            icon="material-symbols:search"
            className="text-lg text-gray-400 foucs:none"
            rotate={1}
          />
          <RhInput type="text" placeholder="search" className=" border-none" />
        </div>
        {/* </RhInputGroup> */}
        <RhIcon
          icon="bi:filter-left"
          className="text-3xl border-4 border-slate-600 font-bold text-black ml-1 h-full w-10"
        />
      </div>
      <RhButton className="bg-black ">+ Add New Project</RhButton>
    </div>
    // </div>
  );
};

export default WithSearch;
