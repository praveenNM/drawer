import React from "react";
import PicSelect from "./Pages/pictureSelection";
import PicShow from "./Pages/pictureShowCase";
import NameSelect from "./Pages/nameSelectionDrawer";
import Projectsession from "./Pages/projectSessions";
import Projectfilter from "./Pages/projectFilter";
import PictureDownloader from "./Pages/pictureDownloader";
import Login from "./Pages/login";
import Registaion from "./Pages/registaion";
import ForgotPassword from "./Pages/forgotPassword";
import EmailSent from "./Pages/emailSent";
import ConfirmEmail from "./Pages/confirmEmail";
import AddNewPro from "./components/Project-assets/2.1.5/AddNewPro2.1.5";
import Processing from "./components/Project-assets/3.2.3/Processing";
import {
	BrowserRouter as Router,
	Route,
	Link,
	createBrowserRouter,
	createRoutesFromElements,
	RouterProvider,
	Outlet,
} from "react-router-dom";

export default function App() {
	const router = createBrowserRouter(
		createRoutesFromElements(
			<Route>
				<Route path="/" element={<Login />} />
				<Route path="/Registration" element={<Registaion />} />
				<Route path="/forgotpassword" element={<ForgotPassword />} />
				<Route path="/emailSent" element={<EmailSent />} />
				<Route path="/confirmemail" element={<ConfirmEmail />} />
				<Route path="/projectfilter" element={<Projectfilter />} />
				<Route path="/ad" element={<AddNewPro />} />
				<Route path="/pr" element={<Processing />} />


				{/* <Route path="/Home" element={<Root />} />
				<Route path="/pictureselection" element={<PicSelect />} />
				<Route path="/pictureshowcase" element={<PicShow />} />
				<Route path="/nameselection" element={<NameSelect />} />
				<Route path="/projectsession" element={<Projectsession />} />
				<Route path="/picturedownloader" element={<PictureDownloader />} /> */}
			</Route>
		)
	);
	return (
		<div>
			<RouterProvider router={router} />
		</div>
	);
}
const Root = () => {
	return (
		<>
			<div className="flex justify-around ">
				<Link to="/"></Link>
				{/* <link to='/Registration'><Registaion/></link> */}

				{/* <Link to="/"></Link>
        <Link to="/pictureselection">Picture-Selection Page</Link>
        <Link to="/pictureshowcase">Picture-ShowCase Page</Link>
        <Link to="/nameselection">Name-selection Page</Link>
        <Link to="/Projectsession">Project-Session</Link>
        <Link to="/projectfilter">Project-Filter</Link>
        <Link to="/picturedownloader">Picture-Downloader</Link> */}
			</div>
			<Outlet />
		</>
	);
};
